import React, { useState, useEffect } from 'react';

import Login from './components/Login/Login';
import Home from './components/Home/Home';
import MainHeader from './components/MainHeader/MainHeader';

const LOGIN_TOKEN = 'isLoggedIn';
const LOGGED_IN = '1';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(
    () => {
      const currentLoginState = localStorage.getItem(LOGIN_TOKEN);
      if (currentLoginState === LOGGED_IN) {
        setIsLoggedIn(true);
      }
    },
    [],
  );

  const loginHandler = (email, password) => {
    // We should of course check email and password
    // But it's just a dummy/ demo anyways
    localStorage.setItem(LOGIN_TOKEN, LOGGED_IN);
    setIsLoggedIn(true);
  };

  const logoutHandler = () => {
    setIsLoggedIn(false);
    localStorage.removeItem(LOGIN_TOKEN);
  };

  return (
    <React.Fragment>
      <MainHeader isAuthenticated={isLoggedIn} onLogout={logoutHandler} />
      <main>
        {!isLoggedIn && <Login onLogin={loginHandler} />}
        {isLoggedIn && <Home onLogout={logoutHandler} />}
      </main>
    </React.Fragment>
  );
}

export default App;
